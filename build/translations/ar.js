(function(d){d['ar']=Object.assign(d['ar']||{},{a:"لا يمكن رفع الملف:",b:"Image toolbar",c:"Table toolbar",d:"عريض",e:"مائل",f:"اقتباس",g:"Insert image or file",h:"اختر عنوان",i:"عنوان",j:"عنصر الصورة",k:"صورة بحجم كامل",l:"صورة جانبية",m:"صورة بمحاذاة لليسار",n:"صورة بالوسط",o:"صورة بمحاذاة لليمين",p:"ادخل عنوان الصورة",q:"ادراج صورة",r:"فشل الرفع",s:"رابط",t:"إدراج جدول",u:"عمود عنوان",v:"Insert column left",w:"Insert column right",x:"حذف العمود",y:"عمود",z:"صف عنوان",aa:"ادراج صف بعد",ab:"ادراج صف قبل",ac:"حذف الصف",ad:"صف",ae:"دمج الخلايا للأعلى",af:"دمج الخلايا لليمين",ag:"دمج الخلايا للأسفل",ah:"دمج الخلايا لليسار",ai:"فصل الخلايا بشكل عمودي",aj:"فصل الخلايا بشكل افقي",ak:"دمج الخلايا",al:"media widget",am:"Insert media",an:"The URL must not be empty.",ao:"This media URL is not supported.",ap:"قائمة رقمية",aq:"قائمة نقطية",ar:"جاري الرفع",as:"Widget toolbar",at:"Could not obtain resized image URL.",au:"Selecting resized image failed",av:"Could not insert image at the current position.",aw:"Inserting image failed",ax:"فقرة",ay:"عنوان 1",az:"عنوان 2",ba:"عنوان 3",bb:"Heading 4",bc:"Heading 5",bd:"Heading 6",be:"غير النص البديل للصورة",bf:"حفظ",bg:"إلغاء",bh:"Paste the media URL in the input.",bi:"Tip: Paste the URL into the content to embed faster.",bj:"Media URL",bk:"Editor toolbar",bl:"Dropdown toolbar",bm:"معالج نصوص",bn:"معالج نصوص، 0%",bo:"%0 of %1",bp:"Previous",bq:"Next",br:"النص البديل",bs:"تراجع",bt:"إعادة",bu:"Open in a new tab",bv:"Downloadable",bw:"إلغاء الرابط",bx:"تحرير الرابط",by:"فتح الرابط في تبويب جديد",bz:"لا يحتوي هذا الرابط على عنوان",ca:"رابط عنوان"})})(window.CKEDITOR_TRANSLATIONS||(window.CKEDITOR_TRANSLATIONS={}));