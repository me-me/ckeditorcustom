(function(d){d['de-ch']=Object.assign(d['de-ch']||{},{a:"Datei kann nicht hochgeladen werden:",b:"Image toolbar",c:"Table toolbar",d:"Bold",e:"Italic",f:"Blockzitat",g:"Insert image or file",h:"Choose heading",i:"Heading",j:"image widget",k:"Full size image",l:"Side image",m:"Left aligned image",n:"Centered image",o:"Right aligned image",p:"Enter image caption",q:"Insert image",r:"Upload failed",s:"Link",t:"Tabelle einfügen",u:"Kopfspalte",v:"Insert column left",w:"Insert column right",x:"Spalte löschen",y:"Spalte",z:"Kopfspalte",aa:"Zeile unten einfügen",ab:"Zeile oben einfügen",ac:"Zeile löschen",ad:"Zeile",ae:"Zelle oben verbinden",af:"Zele rechts verbinden",ag:"Zelle unten verbinden",ah:"Zelle links verbinden",ai:"Zelle vertikal teilen",aj:"Zelle horizontal teilen",ak:"Zellen verbinden",al:"media widget",am:"Insert media",an:"The URL must not be empty.",ao:"This media URL is not supported.",ap:"Numbered List",aq:"Bulleted List",ar:"Upload läuft",as:"Widget toolbar",at:"Could not obtain resized image URL.",au:"Selecting resized image failed",av:"Could not insert image at the current position.",aw:"Inserting image failed",ax:"Paragraph",ay:"Heading 1",az:"Heading 2",ba:"Heading 3",bb:"Heading 4",bc:"Heading 5",bd:"Heading 6",be:"Change image text alternative",bf:"Save",bg:"Cancel",bh:"Paste the media URL in the input.",bi:"Tip: Paste the URL into the content to embed faster.",bj:"Media URL",bk:"Editor toolbar",bl:"Dropdown toolbar",bm:"Rich-Text-Edito",bn:"Rich-Text-Editor, %0",bo:"%0 of %1",bp:"Previous",bq:"Next",br:"Text alternative",bs:"Rückgängig",bt:"Wiederherstellen",bu:"Open in a new tab",bv:"Downloadable",bw:"Unlink",bx:"Edit link",by:"Open link in new tab",bz:"This link has no URL",ca:"Link URL"})})(window.CKEDITOR_TRANSLATIONS||(window.CKEDITOR_TRANSLATIONS={}));